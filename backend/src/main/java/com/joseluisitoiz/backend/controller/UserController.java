package com.joseluisitoiz.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.joseluisitoiz.backend.model.User;
import com.joseluisitoiz.backend.service.UserService;

/**
 * Represents Controller
 * 
 * @author José Luis Itoiz
 * @email joseluisitoiz@hotmail.es
 */
@RestController
public class UserController {

	@Autowired
	private UserService service;

	@PostMapping("/registration")
	@CrossOrigin(origins = "http://localhost:4200")
	public User registerUser(@RequestBody User user) throws Exception {
		if (user.getEmail() != null && !"".equals(user.getEmail())) {
			User userExists = service.fetchUserByEmail(user.getEmail());
			if (userExists != null) {
				throw new Exception("User with email " + userExists.getEmail() + " is already exists.");
			}
		}

		return service.saveUser(user);
	}

	@PostMapping("/login")
	@CrossOrigin(origins = "http://localhost:4200")
	public User loginUser(@RequestBody User user) throws Exception {
		User userExists = null;
		if (user.getEmail() != null && user.getPassword() != null) {
			userExists = service.fetchUserByEmailAndPassword(user.getEmail(), user.getPassword());
		}

		if (userExists == null) {
			throw new Exception("User login error. Bad credentials.");
		}

		return userExists;
	}

	@GetMapping("/users")
	@CrossOrigin(origins = "http://localhost:4200")
	public List<User> findAllUsers() {
		return service.fetchUsers();
	}
}
