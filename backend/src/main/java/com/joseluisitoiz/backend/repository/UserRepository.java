package com.joseluisitoiz.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.joseluisitoiz.backend.model.User;

/**
 * Represents Repository
 * 
 * @author José Luis Itoiz
 * @email joseluisitoiz@hotmail.es
 */
public interface UserRepository extends JpaRepository<User, Integer> {

	public User findByEmail(String email);

	public User findByEmailAndPassword(String email, String password);
}
