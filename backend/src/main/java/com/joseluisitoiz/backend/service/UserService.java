package com.joseluisitoiz.backend.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.joseluisitoiz.backend.model.User;
import com.joseluisitoiz.backend.repository.UserRepository;

/**
 * Represents Service
 * 
 * @author José Luis Itoiz
 * @email joseluisitoiz@hotmail.es
 */
@Service
public class UserService {

	@Autowired
	private UserRepository repository;

	public User saveUser(User user) {
		return repository.save(user);
	}

	public User fetchUserByEmail(String email) {
		return repository.findByEmail(email);
	}

	public User fetchUserByEmailAndPassword(String email, String password) {
		return repository.findByEmailAndPassword(email, password);
	}

	public List<User> fetchUsers() {
		return repository.findAll();
	}
}
