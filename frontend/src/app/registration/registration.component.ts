import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router} from '@angular/router';

import { UserService } from '../user.service';
import { User } from '../user';

@Component({
 	selector: 'app-registration',
 	templateUrl: './registration.component.html',
 	styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  	user = new User();
	msg = '';
	error = '';

  	constructor(private _service: UserService, private _router: Router) { }

	ngOnInit(): void {
	}

	registerUser() {
		this._service.registerUser(this.user).subscribe(
			data => {
				this.msg = 'Usuario registrado correctamente';
			},
			error => {
				console.log(error);
				this.msg = 'Se ha producido un error al registrar el usuario.';
				this.error = error;
			}
		);
	}

}
