import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router} from '@angular/router';

import { UserService } from '../user.service';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	user = new User();
	msg = '';

  	constructor(private _service: UserService, private _router: Router) { }

	ngOnInit(): void {
	}

	login() {
		this._service.loginUser(this.user).subscribe(
			data => {
				this._router.navigate(['/list']);
			},
			error => {
				console.log(error);
				this.msg = "Email y/o contraseña incorrectos."
			}
		);
	}
}
