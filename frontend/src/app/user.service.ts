import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

    private baseUrl = 'http://localhost:8080';

  	constructor( private _http: HttpClient) { }

 	public loginUser(user: User): Observable<any> {
        return this._http.post<any>(`${this.baseUrl}/login`, user)
  	}

  	public registerUser(user: User): Observable<any> {
  		return this._http.post<any>(`${this.baseUrl}/registration`, user)
  	}

   	public getUsers() {
    	return this._http.get<User[]>(`${this.baseUrl}/users`);
  	}
}
