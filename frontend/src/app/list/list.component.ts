import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router} from '@angular/router';

import { UserService } from '../user.service';
import { User } from '../user';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
    users: User[];

    constructor(private _service: UserService, private _router: Router) {}

    ngOnInit(): void {
        this._service.getUsers().subscribe(
        data => {
            this.users = data;
        }
    );
  }
}
